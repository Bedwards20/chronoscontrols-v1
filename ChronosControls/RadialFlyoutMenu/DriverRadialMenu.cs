﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Skins;

namespace ChronosControls
{
    [System.ComponentModel.DefaultProperty("ShowMiniMap,FlyoutAnchorType")]
    [System.ComponentModel.ComplexBindingProperties("Driver_Latitude", "Driver_Longitude")]

    public partial class DriverRadialMenu : DevExpress.XtraEditors.XtraUserControl
    {
        public DriverRadialMenu()
        {
            _defaultmapLocation = new DevExpress.XtraMap.GeoPoint(43.659836D, -79.390514D);
            _mapLocation = new DevExpress.XtraMap.GeoPoint();
            _flyoutAnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.Manual ;
            _flyoutAnimationType = DevExpress.Utils.Win.PopupToolWindowAnimation.Slide;
            
            InitializeComponent();
        }
        
        //private DevExpress.Map.CoordPoint _mapLocation;
        private DevExpress.XtraMap.GeoPoint _mapLocation;
        private DevExpress.XtraMap.GeoPoint _defaultmapLocation;
        private DevExpress.Utils.Win.PopupToolWindowAnchor _flyoutAnchorType;
        private DevExpress.Utils.Win.PopupToolWindowAnimation _flyoutAnimationType;
        private Boolean _defaultShowMiniMap;
        private String _EmailAddress;

        #region "Databinding Properties"
        public string Driver_Latitude
        {
            get { return Convert.ToString( _mapLocation.Latitude); }
            set
            {
                _mapLocation.Latitude = Convert.ToDouble(value);
                set_Map_centerpoint();
            }
        }

        public string Driver_Longitude
        {
            get { return Convert.ToString ( _mapLocation.Longitude); }
            set
            {
                _mapLocation.Longitude =  Convert.ToDouble(value);
                set_Map_centerpoint();
            }
        }

        public String Driver_Email
        {
            get { return _EmailAddress; }
            set { 
                _EmailAddress = value;
                lblEmailAddress.Text = _EmailAddress;
                }
        }






        #endregion

        #region "Public Properties"
        public DevExpress.XtraMap.GeoPoint DefaultmapLocation
        {
            set { _defaultmapLocation = value; }
        }

        [Category("Appearance")]
        [Description("Show/Hide the mini map on location map")]

        public Boolean ShowMiniMap
        {
            get { return _defaultShowMiniMap; }
            set
            {
                _defaultShowMiniMap = value;
                this.LocationMap.MiniMap.Visible = _defaultShowMiniMap;
            }
        }

        [Description("Set Anchor Type for Flyout")]
        private DevExpress.Utils.Win.PopupToolWindowAnchor FlyoutAnchorType
        {
            get {return _flyoutAnchorType;}
            set
            {
                _flyoutAnchorType = value;
                this.LeftFlyoutPanel.Options.AnchorType = _flyoutAnchorType;
            }
        }

        [Description("Set Animation Type for Flyout")]
        public DevExpress.Utils.Win.PopupToolWindowAnimation FlyoutAnimationType
        {
            get { return _flyoutAnimationType; }
            set 
            {
                _flyoutAnimationType = value; 
                this.LeftFlyoutPanel.Options.AnimationType = _flyoutAnimationType;
            }
        }


        #endregion
                
        private void ControlButton_Click(object sender, EventArgs e)
        {

            //Show the Radial Menu
            ShowRadialMenu();

            //Prep control for Showing the Flyout
            if (FlyoutAnchorType == DevExpress.Utils.Win.PopupToolWindowAnchor.Manual)
            {
                //Manual is used at all times, and determines where the left and right flyouts come from
                //Set position relative to Topmost Parent

                LeftFlyoutPanel.OwnerControl = this.ParentForm;

                Point ControltoScreen = this.PointToScreen(Point.Empty);
                Point TLCPoint = this.TopLevelControl.PointToScreen(Point.Empty);

                ControltoScreen.Offset(-TLCPoint.X - (LeftFlyoutPanelControl.Width), -TLCPoint.Y - (LeftFlyoutPanelControl.Height / 2) + (this.Height / 2));
                
                
                LeftFlyoutPanel.Options.Location = ControltoScreen;
                RightFlyoutPanel.Options.Location = ControltoScreen;

            }

            SetFlyoutBackColor();

            //Set Flyout Mapping data
            set_Map_centerpoint();
            set_Driver_Pin();
        }



        #region "Handle Radial Menu Events"
        private void ShowRadialMenu()
        {
            //Get position of the button and center the radial menu at this point
            Point pt = this.ControlButton.PointToScreen(Point.Empty);

            pt.Offset(this.ControlButton.Width / 2, this.ControlButton.Height / 2);
            this.radialMenu.AutoExpand = true;

            //Show Radial Menu at center of button
            this.radialMenu.ShowPopup(pt);
        }

        private void RadialMenubbi_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraBars.BarButtonItem BarButton = (DevExpress.XtraBars.BarButtonItem)e.Item;
            
            

            Point ControltoScreen = this.PointToScreen(Point.Empty);
            Point TLCPoint = this.TopLevelControl.PointToScreen(Point.Empty);

            switch (BarButton.Tag.ToString())
            { 
                case "Right": //Email
                    //Slide Top Left
                    RightFlyoutPanel.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.TopLeft;
                    RightFlyoutPanel.Options.HorzIndent = ((ControltoScreen.X - TLCPoint.X)) - ((RightFlyoutPanelControl.Width / 2) - (this.Width / 2) - 11) + RightFlyoutPanelControl.Width - 11;
                    RightFlyoutPanel.Options.VertIndent = ((ControltoScreen.Y - TLCPoint.Y)) - (RightFlyoutPanel.Height / 2) + (this.Height / 2);
                    RightFlyoutPanel.OwnerControl = this.TopLevelControl;
                    StartFlyout(RightFlyoutPanel);
                    break;
                case "Left": //Location
                    //Slide Top Right
                    LeftFlyoutPanel.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.TopRight;
                    LeftFlyoutPanel.Options.HorzIndent = (this.TopLevelControl.Width - (ControltoScreen.X - TLCPoint.X)) + (LeftFlyoutPanelControl.Width / 2) - (this.Width / 2) - 11;
                    LeftFlyoutPanel.Options.VertIndent = ((ControltoScreen.Y - TLCPoint.Y)) - (LeftFlyoutPanelControl.Height / 2) + (this.Height / 2);
                    LeftFlyoutPanel.OwnerControl = this.TopLevelControl;
                    StartFlyout(LeftFlyoutPanel);
                    break;
                case "Top": //SMS
                    break;
                case "Bottom": //Phone Direct
                    break;
            
            }
            
        }

        private void radialMenu_CloseUp(object sender, EventArgs e)
        {
            RightFlyoutPanel.HidePopup(true);
            LeftFlyoutPanel.HidePopup(true);
        }

        #endregion

        #region "Get RadialMenu outside border color"
        private void SetFlyoutBackColor()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            Color Matchingcolor = skin.Colors[RibbonSkins.SkinRadialMenuColor];


            Skin currentSkin = DevExpress.Skins.RibbonSkins.GetSkin(this.LookAndFeel);
            string elementName = DevExpress.Skins.RibbonSkins.SkinGalleryPane;
            string SubElementName = DevExpress.Skins.RibbonSkins.SkinRadialMenuColor;


            SkinElement element = currentSkin[elementName];
            Color RadMenuColor = element.Color.Owner.Colors.GetColor(SubElementName);

            Color NewMatchingcolor = TransformColor(Matchingcolor, 50);
            LeftFlyoutPanelControl.BackColor = NewMatchingcolor;
            LeftFlyoutPanel.BackColor = NewMatchingcolor;

            RightFlyoutPanelControl.BackColor = NewMatchingcolor;
            RightFlyoutPanel.BackColor = NewMatchingcolor;


            //MapPanelControlHolder.BackColor = NewMatchingcolor;
        }
        protected Color TransformColor(Color color, int delta)
        {
            return Color.FromArgb(CheckColorRange(color.R + delta), CheckColorRange(color.G + delta), CheckColorRange(color.B + delta));
        }
        protected int CheckColorRange(int value)
        {
            if (value < 0) return 0;
            if (value > 255) return 255;
            return value;
        }
        #endregion

        #region "Handle Flyout from Radial Menu

        private void StartFlyout(RadialFlyoutMenu.MyFlyoutPanel sender)
        {
            if (!sender.IsPopupOpen) { sender.ShowPopup(); } else { sender.HidePopup(); }
        }
        #endregion

        #region "Mapping Control"

        private void set_Map_centerpoint()
        {
            //use Databinding location for map
            if (_mapLocation.Latitude != 0 && _mapLocation.Longitude != 0)
            {
                LocationMap.SetCenterPoint(_mapLocation, false);
                LocationMap.ZoomLevel = 14;
            }
            else //use default Location for map
            {
                LocationMap.SetCenterPoint(_defaultmapLocation, false);
                LocationMap.ZoomLevel = 7;
            }
        }

        private void set_Driver_Pin()
        {
            DevExpress.XtraMap.MapItemStorage DriverMapItemStorage = new DevExpress.XtraMap.MapItemStorage();
            DevExpress.XtraMap.MapPushpin DriverMapPushpin = new DevExpress.XtraMap.MapPushpin();
            DevExpress.XtraMap.VectorItemsLayer DriverVectorItemsLayer = new DevExpress.XtraMap.VectorItemsLayer();

            DriverMapPushpin.Location = _mapLocation;
            DriverMapItemStorage.Items.Add(DriverMapPushpin);
            DriverVectorItemsLayer.Data = DriverMapItemStorage;
            DriverVectorItemsLayer.ItemImageIndex = 4;
            this.LocationMap.Layers.Add(DriverVectorItemsLayer);
        }

        #endregion

        private void DriverRadialMenu_Load(object sender, EventArgs e)
        {

        }

        #region "Email Control"
        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            //Send Email

            //Clear Email Message
            mEditEmailMessage.EditValue = "";
            //Hide Email Panel
            StartFlyout(RightFlyoutPanel);
        }
        #endregion


    }
}
