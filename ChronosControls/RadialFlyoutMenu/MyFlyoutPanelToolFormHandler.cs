﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Utils;
using DevExpress.Utils.Win;

namespace ChronosControls.RadialFlyoutMenu
{
    public class MyFlyoutPanelToolFormHandler : FlyoutPanelToolFormHandler
    {
        public MyFlyoutPanelToolFormHandler(FlyoutPanelToolForm toolForm)
            : base(toolForm)
        {
        }
        protected override PopupToolWindowAnimationProviderBase CreateAnimationProvider()
        {
            if (Anchor == PopupToolWindowAnchor.TopLeft)
            {
                return new MyPopupToolWindowLeftRightSlideAnimationProvider(this);
            }
            if (Anchor == PopupToolWindowAnchor.TopRight)
                return new MyPopupToolWindowRightLeftSlideAnimationProvider(this);
            return base.CreateAnimationProvider();
        }
        protected PopupToolWindowAnchor Anchor
        {
            get
            {
                return ((IPopupToolWindowAnimationSupports)this).AnchorType;
            }
        }
    }
}