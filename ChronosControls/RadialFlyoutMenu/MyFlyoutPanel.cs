﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;

namespace ChronosControls.RadialFlyoutMenu
{
    public class MyFlyoutPanel : FlyoutPanel {
        protected override FlyoutPanelToolForm CreateToolFormCore(Control owner, FlyoutPanel content, FlyoutPanelOptions options) {
            return new MyFlyoutPanelToolForm(owner, content, options);
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MyFlyoutPanel
            // 
            this.Name = "MyFlyoutPanel";
            this.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.Manual;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
