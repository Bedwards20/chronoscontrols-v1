﻿namespace ChronosControls
{
    partial class DriverRadialMenu : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DriverRadialMenu));
            DevExpress.XtraMap.ImageTilesLayer imageTilesLayer1 = new DevExpress.XtraMap.ImageTilesLayer();
            DevExpress.XtraMap.BingMapDataProvider bingMapDataProvider1 = new DevExpress.XtraMap.BingMapDataProvider();
            DevExpress.XtraMap.InformationLayer informationLayer1 = new DevExpress.XtraMap.InformationLayer();
            DevExpress.XtraMap.BingGeocodeDataProvider bingGeocodeDataProvider1 = new DevExpress.XtraMap.BingGeocodeDataProvider();
            DevExpress.XtraMap.MiniMap miniMap1 = new DevExpress.XtraMap.MiniMap();
            DevExpress.XtraMap.DynamicMiniMapBehavior dynamicMiniMapBehavior1 = new DevExpress.XtraMap.DynamicMiniMapBehavior();
            this.ControlButton = new System.Windows.Forms.PictureBox();
            this.imgColRadMenu = new DevExpress.Utils.ImageCollection(this.components);
            this.radialMenu = new DevExpress.XtraBars.Ribbon.RadialMenu(this.components);
            this.bbiSMS = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEmail = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPhone = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLocation = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.LeftFlyoutPanel = new ChronosControls.RadialFlyoutMenu.MyFlyoutPanel();
            this.LeftFlyoutPanelControl = new DevExpress.Utils.FlyoutPanelControl();
            this.LocationMap = new DevExpress.XtraMap.MapControl();
            this.pnlEmail = new DevExpress.XtraEditors.PanelControl();
            this.lblEmailAddress = new DevExpress.XtraEditors.LabelControl();
            this.mEditEmailMessage = new DevExpress.XtraEditors.MemoEdit();
            this.btnSendEmail = new DevExpress.XtraEditors.SimpleButton();
            this.lblEmailTo = new DevExpress.XtraEditors.LabelControl();
            this.RightFlyoutPanel = new ChronosControls.RadialFlyoutMenu.MyFlyoutPanel();
            this.RightFlyoutPanelControl = new DevExpress.Utils.FlyoutPanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.ControlButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgColRadMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radialMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftFlyoutPanel)).BeginInit();
            this.LeftFlyoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LeftFlyoutPanelControl)).BeginInit();
            this.LeftFlyoutPanelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlEmail)).BeginInit();
            this.pnlEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mEditEmailMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightFlyoutPanel)).BeginInit();
            this.RightFlyoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RightFlyoutPanelControl)).BeginInit();
            this.RightFlyoutPanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // ControlButton
            // 
            this.ControlButton.Image = global::ChronosControls.Properties.Resources.people_delivery_man;
            this.ControlButton.Location = new System.Drawing.Point(0, 0);
            this.ControlButton.Margin = new System.Windows.Forms.Padding(0);
            this.ControlButton.Name = "ControlButton";
            this.ControlButton.Size = new System.Drawing.Size(16, 16);
            this.ControlButton.TabIndex = 0;
            this.ControlButton.TabStop = false;
            this.ControlButton.Click += new System.EventHandler(this.ControlButton_Click);
            // 
            // imgColRadMenu
            // 
            this.imgColRadMenu.ImageSize = new System.Drawing.Size(24, 24);
            this.imgColRadMenu.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgColRadMenu.ImageStream")));
            this.imgColRadMenu.Images.SetKeyName(0, "location_red.png");
            this.imgColRadMenu.Images.SetKeyName(1, "comment.png");
            this.imgColRadMenu.Images.SetKeyName(2, "mail.png");
            this.imgColRadMenu.Images.SetKeyName(3, "phone.png");
            this.imgColRadMenu.Images.SetKeyName(4, "people_delivery_man.png");
            // 
            // radialMenu
            // 
            this.radialMenu.CloseOnOuterMouseClick = false;
            this.radialMenu.CollapseOnOuterMouseClick = false;
            this.radialMenu.Glyph = global::ChronosControls.Properties.Resources.people_delivery_man1;
            this.radialMenu.ItemAutoSize = DevExpress.XtraBars.Ribbon.RadialMenuItemAutoSize.Spring;
            this.radialMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSMS),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEmail),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPhone),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLocation)});
            this.radialMenu.Manager = this.barManager;
            this.radialMenu.MenuRadius = 135;
            this.radialMenu.Name = "radialMenu";
            this.radialMenu.CloseUp += new System.EventHandler(this.radialMenu_CloseUp);
            // 
            // bbiSMS
            // 
            this.bbiSMS.Caption = "SMS";
            this.bbiSMS.Id = 0;
            this.bbiSMS.ImageIndex = 1;
            this.bbiSMS.Name = "bbiSMS";
            this.bbiSMS.Tag = "Top";
            // 
            // bbiEmail
            // 
            this.bbiEmail.Caption = "Email";
            this.bbiEmail.Id = 1;
            this.bbiEmail.ImageIndex = 2;
            this.bbiEmail.Name = "bbiEmail";
            this.bbiEmail.Tag = "Right";
            this.bbiEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.RadialMenubbi_Click);
            // 
            // bbiPhone
            // 
            this.bbiPhone.Caption = "Phone";
            this.bbiPhone.Id = 2;
            this.bbiPhone.ImageIndex = 3;
            this.bbiPhone.Name = "bbiPhone";
            this.bbiPhone.Tag = "Bottom";
            // 
            // bbiLocation
            // 
            this.bbiLocation.Caption = "Location";
            this.bbiLocation.Id = 3;
            this.bbiLocation.ImageIndex = 0;
            this.bbiLocation.Name = "bbiLocation";
            this.bbiLocation.Tag = "Left";
            this.bbiLocation.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.RadialMenubbi_Click);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Images = this.imgColRadMenu;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSMS,
            this.bbiEmail,
            this.bbiPhone,
            this.bbiLocation});
            this.barManager.MaxItemId = 4;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(16, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 16);
            this.barDockControlBottom.Size = new System.Drawing.Size(16, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 16);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(16, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 16);
            // 
            // LeftFlyoutPanel
            // 
            this.LeftFlyoutPanel.Appearance.BackColor = System.Drawing.Color.Blue;
            this.LeftFlyoutPanel.Appearance.Options.UseBackColor = true;
            this.LeftFlyoutPanel.Controls.Add(this.LeftFlyoutPanelControl);
            this.LeftFlyoutPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftFlyoutPanel.Name = "LeftFlyoutPanel";
            this.LeftFlyoutPanel.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.TopRight;
            this.LeftFlyoutPanel.OptionsBeakPanel.BackColor = System.Drawing.Color.Blue;
            this.LeftFlyoutPanel.Size = new System.Drawing.Size(216, 143);
            this.LeftFlyoutPanel.TabIndex = 5;
            this.LeftFlyoutPanel.Tag = "Left";
            // 
            // LeftFlyoutPanelControl
            // 
            this.LeftFlyoutPanelControl.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.LeftFlyoutPanelControl.Appearance.Options.UseBackColor = true;
            this.LeftFlyoutPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LeftFlyoutPanelControl.Controls.Add(this.LocationMap);
            this.LeftFlyoutPanelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftFlyoutPanelControl.FlyoutPanel = this.LeftFlyoutPanel;
            this.LeftFlyoutPanelControl.Location = new System.Drawing.Point(0, 0);
            this.LeftFlyoutPanelControl.Name = "LeftFlyoutPanelControl";
            this.LeftFlyoutPanelControl.Size = new System.Drawing.Size(216, 143);
            this.LeftFlyoutPanelControl.TabIndex = 0;
            // 
            // LocationMap
            // 
            this.LocationMap.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.LocationMap.CenterPoint = new DevExpress.XtraMap.GeoPoint(43.7D, -79.4D);
            this.LocationMap.Font = new System.Drawing.Font("Tahoma", 6F);
            bingMapDataProvider1.BingKey = "RmQcsBvrsRu7tzTr5aLF~QDfRvmz-iU34liKsN9geVg~AhF8u7f-2QVMTxwhnQuKDVN1hZJ8uyq5M7ubb" +
    "HsUvgbMIZdghUvrxmxZ6so0VMdA";
            bingMapDataProvider1.Kind = DevExpress.XtraMap.BingMapKind.Road;
            bingMapDataProvider1.TileSource = null;
            imageTilesLayer1.DataProvider = bingMapDataProvider1;
            bingGeocodeDataProvider1.BingKey = "RmQcsBvrsRu7tzTr5aLF~QDfRvmz-iU34liKsN9geVg~AhF8u7f-2QVMTxwhnQuKDVN1hZJ8uyq5M7ubb" +
    "HsUvgbMIZdghUvrxmxZ6so0VMdA";
            bingGeocodeDataProvider1.MaxVisibleResultCount = 15;
            bingGeocodeDataProvider1.ProcessMouseEvents = false;
            informationLayer1.DataProvider = bingGeocodeDataProvider1;
            informationLayer1.HighlightedItemStyle.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            informationLayer1.ItemStyle.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            informationLayer1.SelectedItemStyle.Font = new System.Drawing.Font("Tahoma", 6F, System.Drawing.FontStyle.Bold);
            this.LocationMap.Layers.Add(imageTilesLayer1);
            this.LocationMap.Layers.Add(informationLayer1);
            this.LocationMap.Location = new System.Drawing.Point(17, 19);
            miniMap1.Behavior = dynamicMiniMapBehavior1;
            this.LocationMap.MiniMap = miniMap1;
            this.LocationMap.Name = "LocationMap";
            this.LocationMap.NavigationPanelOptions.CoordinatesStyle.Font = new System.Drawing.Font("Tahoma", 8F);
            this.LocationMap.NavigationPanelOptions.Height = 30;
            this.LocationMap.Size = new System.Drawing.Size(199, 105);
            this.LocationMap.TabIndex = 0;
            // 
            // pnlEmail
            // 
            this.pnlEmail.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.pnlEmail.Appearance.Options.UseBackColor = true;
            this.pnlEmail.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlEmail.Controls.Add(this.lblEmailAddress);
            this.pnlEmail.Controls.Add(this.mEditEmailMessage);
            this.pnlEmail.Controls.Add(this.btnSendEmail);
            this.pnlEmail.Controls.Add(this.lblEmailTo);
            this.pnlEmail.Location = new System.Drawing.Point(11, 19);
            this.pnlEmail.Name = "pnlEmail";
            this.pnlEmail.Size = new System.Drawing.Size(188, 105);
            this.pnlEmail.TabIndex = 11;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.Location = new System.Drawing.Point(40, 5);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(0, 13);
            this.lblEmailAddress.TabIndex = 4;
            // 
            // mEditEmailMessage
            // 
            this.mEditEmailMessage.EditValue = "";
            this.mEditEmailMessage.Location = new System.Drawing.Point(20, 22);
            this.mEditEmailMessage.MenuManager = this.barManager;
            this.mEditEmailMessage.Name = "mEditEmailMessage";
            this.mEditEmailMessage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mEditEmailMessage.Properties.Appearance.Options.UseFont = true;
            this.mEditEmailMessage.Properties.NullValuePrompt = "Enter your message here.";
            this.mEditEmailMessage.Properties.NullValuePromptShowForEmptyValue = true;
            this.mEditEmailMessage.Size = new System.Drawing.Size(163, 60);
            this.mEditEmailMessage.TabIndex = 3;
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Location = new System.Drawing.Point(130, 84);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(53, 18);
            this.btnSendEmail.TabIndex = 2;
            this.btnSendEmail.Text = "Send";
            this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
            // 
            // lblEmailTo
            // 
            this.lblEmailTo.Location = new System.Drawing.Point(20, 5);
            this.lblEmailTo.Name = "lblEmailTo";
            this.lblEmailTo.Size = new System.Drawing.Size(16, 13);
            this.lblEmailTo.TabIndex = 0;
            this.lblEmailTo.Text = "To:";
            // 
            // RightFlyoutPanel
            // 
            this.RightFlyoutPanel.Appearance.BackColor = System.Drawing.Color.Blue;
            this.RightFlyoutPanel.Appearance.Options.UseBackColor = true;
            this.RightFlyoutPanel.Controls.Add(this.RightFlyoutPanelControl);
            this.RightFlyoutPanel.Location = new System.Drawing.Point(0, 0);
            this.RightFlyoutPanel.Name = "RightFlyoutPanel";
            this.RightFlyoutPanel.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.TopRight;
            this.RightFlyoutPanel.OptionsBeakPanel.BackColor = System.Drawing.Color.Blue;
            this.RightFlyoutPanel.Size = new System.Drawing.Size(216, 143);
            this.RightFlyoutPanel.TabIndex = 6;
            this.RightFlyoutPanel.Tag = "Right";
            // 
            // RightFlyoutPanelControl
            // 
            this.RightFlyoutPanelControl.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.RightFlyoutPanelControl.Appearance.Options.UseBackColor = true;
            this.RightFlyoutPanelControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.RightFlyoutPanelControl.Controls.Add(this.pnlEmail);
            this.RightFlyoutPanelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightFlyoutPanelControl.FlyoutPanel = this.RightFlyoutPanel;
            this.RightFlyoutPanelControl.Location = new System.Drawing.Point(0, 0);
            this.RightFlyoutPanelControl.Name = "RightFlyoutPanelControl";
            this.RightFlyoutPanelControl.Size = new System.Drawing.Size(216, 143);
            this.RightFlyoutPanelControl.TabIndex = 0;
            // 
            // DriverRadialMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RightFlyoutPanel);
            this.Controls.Add(this.LeftFlyoutPanel);
            this.Controls.Add(this.ControlButton);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximumSize = new System.Drawing.Size(16, 16);
            this.MinimumSize = new System.Drawing.Size(16, 16);
            this.Name = "DriverRadialMenu";
            this.Size = new System.Drawing.Size(16, 16);
            this.Load += new System.EventHandler(this.DriverRadialMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ControlButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgColRadMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radialMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftFlyoutPanel)).EndInit();
            this.LeftFlyoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LeftFlyoutPanelControl)).EndInit();
            this.LeftFlyoutPanelControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LocationMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlEmail)).EndInit();
            this.pnlEmail.ResumeLayout(false);
            this.pnlEmail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mEditEmailMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightFlyoutPanel)).EndInit();
            this.RightFlyoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RightFlyoutPanelControl)).EndInit();
            this.RightFlyoutPanelControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ControlButton;
        private DevExpress.Utils.ImageCollection imgColRadMenu;
        private DevExpress.XtraBars.Ribbon.RadialMenu radialMenu;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbiSMS;
        private DevExpress.XtraBars.BarButtonItem bbiEmail;
        private DevExpress.XtraBars.BarButtonItem bbiPhone;
        private DevExpress.XtraBars.BarButtonItem bbiLocation;
        private RadialFlyoutMenu.MyFlyoutPanel LeftFlyoutPanel;
        private DevExpress.Utils.FlyoutPanelControl LeftFlyoutPanelControl;
        private DevExpress.XtraMap.MapControl LocationMap;
        private DevExpress.XtraEditors.PanelControl pnlEmail;
        private DevExpress.XtraEditors.MemoEdit mEditEmailMessage;
        private DevExpress.XtraEditors.SimpleButton btnSendEmail;
        private DevExpress.XtraEditors.LabelControl lblEmailTo;
        private RadialFlyoutMenu.MyFlyoutPanel RightFlyoutPanel;
        private DevExpress.Utils.FlyoutPanelControl RightFlyoutPanelControl;
        private DevExpress.XtraEditors.LabelControl lblEmailAddress;
    }
}
